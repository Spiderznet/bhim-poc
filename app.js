var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

const requestTimeMiddleware = (req, res, next) => {
    const startTime = new Date();
    res.on('finish', () => {
        const endTime = new Date();
        const diff = endTime.getTime() - startTime.getTime()
        // tslint:disable-next-line:no-console
        console.log(`${(diff)}ms`)
    })
    next()
}

app.use(requestTimeMiddleware)
// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.get('/api', (req, res, next) => {
    throw new Error('No records found')
    res.send({
        message: 'API working fine!'
    })
})

const handleRuntimeException = (err, req, res, next) => {
    console.log(err)
    res.send({
        status: false,
        message: 'Something went wrong!'
    })
}

app.use(handleRuntimeException)

app.use

module.exports = app;
